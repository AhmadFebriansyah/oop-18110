//Nomor 2
/*
public class MyClass18110 {
    int x = 5;
}*/

//Nomor 3
/*
public class MyClass18110 {
    int x = 5;
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110();
    System.out.println(myObj18110.x);
    }
}*/

//Nomor 4
/*
public class MyClass18110 {
    int x = 5;
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110(); // Object 1
    MyClass18110 myObj2 = new MyClass18110(); // Object 2
    System.out.println(myObj18110.x);
    System.out.println(myObj2.x);
   }
}*/

//Nomor 5
/*
public class MyClass18110 {
    int x = 5;
    /*
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110(); // Object 1
    MyClass18110 myObj2 = new MyClass18110(); // Object 2
    System.out.println(myObj18110.x);
    System.out.println(myObj2.x); */
/*} */

//Nomor 6
/*
public class MyClass18110 {
    static void myMethod() {
    System.out.println("Hello World!");
    }
}*/
//Nomor 7
/*
public class MyClass18110 {
    static void myStaticMethod() {
        System.out.println("Static methods can be called without creating objects");
        }
        // metode publik
        public void myPublicMethod() {
        System.out.println("Public methods must be called by creating objects");
        }
        // metode main/utama
        public static void main(String[] args) {
        myStaticMethod(); // Panggil metode statis
        // myPublicMethod(); Ini akan mengkompilasi kesalahan
        MyClass18110 myObj18110 = new MyClass18110(); // Buat objek MyClass
        myObj18110.myPublicMethod(); // Panggil metode publik pada objek tersebut
        }
}*/
//Nomor 10
/*
public class MyClass18110 {
    int x; // buat kelas atribut
    // Buat konstruktor kelas untuk kelas MyClass
        public MyClass18110() {
        x = 5; // Set the initial value for the class attribute x
    }
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110();
    // Create an object of class MyClass (This will call the constructor)
    System.out.println(myObj18110.x); // Print the value of x
}
}*/

//Nomor 11
/*
public class MyClass18110 {
    static void myStaticMethod() {
        System.out.println("Static methods can be called without creating objects");
        }
        // Public method
        public void myPublicMethod() {
        System.out.println("Metode statis dapat dipanggil tanpa membuat objek ");
        }
        // Metode utama
        public static void main(String[ ] args) {
        myStaticMethod(); // panggil metode static
        // myPublicMethod(); This would output an error
        MyClass18110 myObj18110 = new MyClass18110(); // Buat objek MyClass
        myObj18110.myPublicMethod(); // panggil metode public
        }
}*/

//Nomor 11 abstrak
/*
class MyClass18110 {
    public static void main(String[] args) {
    // membuat objek kelas Student (yang mewarisi atribut dan metode dari
   
    Student18110 myObj18110 = new Student18110();
    System.out.println("Name: " + myObj18110.fname);
    System.out.println("Age: " + myObj18110.age);
    System.out.println("Graduation Year: " + myObj18110.graduationYear);
    myObj18110.study(); // panggil metode abstrak
    }
   }*/

//Nomor 11 final
/*
public class MyClass18110 {
    final int x = 10;
    final double PI = 3.14;
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110();
    myObj18110.x = 50; // akan menghasilkan kesalahan: tidak dapat memberikan
    // nilai ke variabel akhir
    myObj18110.PI = 25; // akan menghasilkan kesalahan: tidak dapat memberikan
    // nilai ke variabel akhir
    System.out.println(myObj18110.x);
    }
   }*/

//Nomor 12
/*
public class MyClass18110 {
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110();
    myObj18110.setName("John"); // Tetapkan nilai variabel nama ke "John"
    System.out.println(myObj18110.getName());
    }
}*/

//Nomor 13
/*
import java.util.Scanner;
class MyClass18110 {
    public static void main(String[] args) {
        Scanner myObj18110 = new Scanner(System.in);
        System.out.println("Enter username");
        String userName = myObj18110.nextLine();
        System.out.println("Username is: " + userName);
        }
}*/

//Nomor20
/*
import java.util.Scanner;
class MyClass18110 {
    public static void main(String[] args) {
        Scanner myObj18110 = new Scanner(System.in);
        System.out.println("Enter name, age and salary:");
        String name = myObj18110.nextLine();
        // Numerical input
        int age = myObj18110.nextInt();
        double salary = myObj18110.nextDouble();
        // Output input by user
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Salary: " + salary);
        }
    }*/

//Nomor 21a
/*
import java.time.LocalDate; // import the LocalDate class
public class MyClass18110 {
 public static void main(String[] args) {
 LocalDate myObj18110 = LocalDate.now(); // Create a date object
 System.out.println(myObj18110); // Display the current date
 }
}*/

//Nomor 21b
/*
public class MyClass18110 {
    public static void main(String[] args) {
        LocalTime myObj18110 = LocalTime.now();
        System.out.println(myObj18110);
        }
}*/

//Nomor 21 c
/*
import java.time.LocalDateTime; // import the LocalDateTime class
public class MyClass18110 {
 public static void main(String[] args) {
 LocalDateTime myObj18110 = LocalDateTime.now();
 System.out.println(myObj18110);
 }
}*/

//Nomor 22a
/*
public class MyClass18110 {
    static void myMethod() {
    System.out.println("I just got executed!");
    }
    public static void main(String[] args) {
    myMethod();
    }
   }*/

//Nomor 22b
/*
public class MyClass18110 {
    static void myMethod() {
    System.out.println("I just got executed!");
    }
    public static void main(String[] args) {
    myMethod();
    myMethod();
    myMethod();
    }
   }*/

   //Nomor 23a
   /*
   public class MyClass18110 {
    static void myMethod(String fname) {
    System.out.println(fname + " Refsnes");
    }
    public static void main(String[] args) {
    myMethod("Liam");
    myMethod("Jenny");
    myMethod("Anja");
    }
   }*/
   
   //Nomor 23b
   /*
   public class MyClass18110 {
    static void myMethod(String fname, int age) {
    System.out.println(fname + " is " + age);
    }
    public static void main(String[] args) {
    myMethod("Liam", 5);
    myMethod("Jenny", 8);
    myMethod("Anja", 31);
    }
   }*/
   //Nomor 23c
   /*
   public class MyClass18110 {
    static int myMethod(int x) {
    return 5 + x;
    }
    public static void main(String[] args) {
    System.out.println(myMethod(3));
    }
   }*/

   //Nomor 23d
   /*
public class MyClass18110 {
    static int myMethod(int x, int y) {
    return x + y;
    }
    public static void main(String[] args) {
        System.out.println(myMethod(5, 3));
 }
}*/

   //Nomor 23e
   /*
   public class MyClass18110 {
    static int myMethod(int x, int y) {
    return x + y;
    }
    public static void main(String[] args) {
    int z = myMethod(5, 3);
    System.out.println(z);
    }
   }*/

   //Nomor 23f
   /*
public class MyClass18110 {
    // Create a checkAge() method with an integer variable called age
    static void checkAge(int age) {
    // If age is less than 18, print "access denied"
    if (age < 18) {
    System.out.println("Access denied - You are not old enough!");
} else {
    System.out.println("Access granted - You are old enough!");
    }
    }
    public static void main(String[] args) {
    checkAge(20); // Call the checkAge method and pass along an age of 20
    }
   }*/

   //Nomor 25
   /*
public class MyClass18110 {
    public static void main(String[] args) {
    // Code here CANNOT use x
    int x = 100;
    // Code here can use x
    System.out.println(x);
    }
   }*/

   //Nomor 25a
   /*
public class MyClass18110 {
    public static void main(String[] args) {
    // Code here CANNOT use x
    { // This is a block
    // Code here CANNOT use x
    int x = 100;
    System.out.println(x);
 } // The block ends here
 // Code here CANNOT use x
 }
}*/

   //Nomor 26
   /*
   public class MyClass18110 {
    public static void main(String[] args) {
    int result = sum(10);
    System.out.println(result);
    }
    public static int sum(int k) {
    if (k > 0) {
        return k + sum(k - 1);
 } else {
 return 0;
 }
 }
}*/

   //Nomor 26a
   /*
   public class MyClass18110 {
    public static void main(String[] args) {
    int result = sum(5, 10);
    System.out.println(result);
    }
    public static int sum(int start, int end) {
    if (end > start) {
    return end + sum(start, end - 1);
    } else {
    return end;
    }
    }
   }*/
