//Nomor 5
/*
class OtherClass18110 {
    public static void main(String[] args) {
    MyClass18110 myObj18110 = new MyClass18110();
    System.out.println(myObj18110.x);
    }
   }*/

//Nomor 9
/*
class OtherClass18110 {
    public static void main(String[] args) {
    Car18110 myCar18110 = new Car18110(); // Create a myCar object
    myCar18110.fullThrottle(); // Call the fullThrottle() method
    myCar18110.speed(200); // Call the speed() method
    }
   }*/

//Nomor 16
/*
class OuterClass18110 {
    int x = 10;
    class InnerClass {
    int y = 5;
    }
   }*/

//Nomor 16 akses
/*
class OuterClass18110 {
    int x = 10;
    class InnerClass {
    public int myInnerMethod() {
    return x;
    }
    }
   }*/

//Nomor 16 Private
/*
class OuterClass18110 {
    int x = 10;
    private class InnerClass {
        int y = 5;
        }
       }*/

//Nomor 16 Static
/*
class OuterClass18110 {
    int x = 10;
    static class InnerClass {
    int y = 5;
    }
   }*/