//Nomor 15
/*
class MyMainClass18110 {
    public static void main(String[] args) {
    Animal18110 myAnimal18110 = new Animal18110(); // Create a Animal object
    Animal18110 myPigl18110 = new Pig18110(); // Create a Pig object
    Animal18110 myDogl18110 = new Dog18110(); // Create a Dog object
    myAnimall18110.animalSound();
    myPigl18110.animalSound();
    myDogl18110.animalSound();
    }
   }*/

//Nomor 16
/*
public class MyMainClass18110 {
    public static void main(String[] args) {
    OuterClass18110 myOuterl18110 = new OuterClass18110();
    OuterClass18110.InnerClass myInnerl18110 = myOuterl18110.new InnerClass();
    System.out.println(myInnerl18110.y + myOuterl18110.x);
    }
   }*/

//Nomor 16 Akses
/*
public class MyMainClass18110 {
    public static void main(String[] args) {
    OuterClass_Akses18110 myOuterl18110 = new OuterClass_Akses_16();
    OuterClass_Akses18110.InnerClass myInnerl18110 = myOuterl18110.new InnerClass();
    System.out.println(myInnerl18110.myInnerMethod());
    }
}*/

//Nomor 16 Private
/*
public class MyMainClass18110 {
    public static void main(String[] args) {
    OuterClass18110 myOuterl18110 = new OuterClass18110();
    OuterClass18110.InnerClass myInnerl18110 = myOuterl18110.new InnerClass();
    System.out.println(myInnerl18110.y + myOuterl18110.x);
    }
   }*/

//Nomor 16 static
/*
public class MyMainClass18110 {
    public static void main(String[] args) {
    OuterClass181110.InnerClass myInnerl18110 = new OuterClass18110.InnerClass();
    System.out.println(myInnerl18110.y);
    }
   }*/

//Nomor 17
/*
class MyMainClass18110 {
    public static void main(String[] args) {
    Pig18110 myPigl18110 = new Pig18110(); // Create a Pig object
    myPigl18110.animalSound();
    myPigl18110.sleep();
    }
   }*/

//Nomor 18
/*
class MyMainClass_18 {
    public static void main(String[] args) {
    Pig18110 myPigl18110 = new Pigl18110(); // Create a Pig object
    myPigl18110.animalSound();
    myPigl18110.sleep();
    }
   }*/

//Nomor 18a
/*
class MyMainClass18110 {
    public static void main(String[] args) {
    DemoClassl18110 myObjl18110 = new DemoClassl18110();
    myObjl18110.myMethod();
    myObjl18110.myOtherMethod();
    }
   }*/